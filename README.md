# Basics of Running FHI-aims

Basics of running a simulation with FHI-aims. Please visit the following page to read the detailed instructions:

https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/

## What you can find here

All of the material for the tutorial can be found in the folder `tutorial`. The `tutorial` folder contains the same structure as the tutorial itself. In addition, you'll find a `solutions` folder in each of the tutorial parts. The `solutions` folder contains the full in- and output for each calcualtion, so you can directly comapre with your results. 

The folder strucutre

```
Tutorial
├── 1-Molecules
│   └── solutions
│       ├── H2O-HSE06-relax-no-restart
│       ├── H2O-HSE06-relax-restart
│       └── H2O-PBE-relax
├── 2-Spin-polarized-Systems
│   └── solutions
│       └── O2
├── 3-Periodic-Systems
│   ├── img
│   └── solutions
│       ├── GaAs
│       │   ├── bands_dos_SOC
│       │   ├── HSE06_followup_relaxation
│       │   ├── Mulliken_bands_SOC
│       │   └── PBE_relaxation
│       └── Si
│           ├── HSE06_bands_dos
│           ├── HSE06_followup_relaxation
│           ├── HSE06_followup_relaxation_intermediate
│           └── PBE_relaxation
├── older_versions
│   ├── 210716_2
│   └── 240507
└── Summary
    └── images
```
