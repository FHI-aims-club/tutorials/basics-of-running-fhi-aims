*[BFGS]: Broyden–Fletcher–Goldfarb–Shanno
*[DFT]: Density-Functional Theory
*[GGA]: Generalized-Gradient Approximation
*[HSE06]: Heyd-Scuseria-Ernzerhof functional, version 2006
*[PBE]: Perdew-Burke-Ernzerhof functional
*[SOC]: Spin-orbit coupling
*[XC]: exchange-correlation