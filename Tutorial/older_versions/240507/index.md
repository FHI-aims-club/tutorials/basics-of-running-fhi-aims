# Basics of Running FHI-aims 

!!! warning "Disclaimer"

    This is an older tutorial version and was made for FHI-aims version `240507`. For the newest version of this tutorial, please click [here](../../index.md).

Welcome to this introductory tutorial on how to use the [FHI-aims](https://fhi-aims.org) code for simulations of atoms, molecules, and solids. 

In order to successfully execute this tutorial, you should have access to the two primary community resources of the FHI-aims project:

- The FHI-aims development and community server <https://aims-git.rz-berlin.mpg.de>
- The FHI-aims slack workspace <https://fhi-aims.slack.com>

Any user of FHI-aims is encouraged to request access to both resources (please follow the instructions at <https://fhi-aims.org/get-the-code>).

All files related to the tutorial, including solutions, can be found at <https://gitlab.com/FHI-aims-club/tutorials/basics-of-running-fhi-aims>.

A current copy of the FHI-aims manual can be found at <https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/jobs/artifacts/master/raw/doc/manual/FHI-aims.pdf?job=build.manual>.

And yes, FHI-aims is a community code. Without its large community of contributors (see <https://fhi-aims.org/who-we-are>), FHI-aims would not exist. We are immensely grateful to the very large number of individuals who have contributed to the code over time and who continue to push it forward.

!!! note "Older versions of this tutorial"

    For FHI-aims versions prior to `240507`, please refer to the [older version of this tutorial](../210716_2/index.md).

## The Objective

This tutorial introduces the FHI-aims software package and the fundamentals of how to run DFT calculations for atoms, molecule and solids. You will learn how to start such simulations and how to generate output files and to extract physical observable from those output files. 

On the technical side, the tutorial will introduce:

- FHI-aims input files `geometry.in` and `control.in`
- the FHI-aims species defaults
- running FHI-aims in parallel
- structure of the main output file
- structure optimization (aka relaxation)
- difference for systems with and without (collinear) spin
- structure relaxation for non-periodic and periodic systems

For these questions, FHI-aims has an advantage in that it can deal with non-periodic structures and with periodic solids and surfaces on exactly equal footing, simply by including or omitting unit cell vectors.

## Prerequisites

Users of this tutorial should have:

- a sufficiently good understanding of the Unix command line,
- an installed FHI-aims executable (cf. preparations),
- access to a sufficiently powerful computer. For this tutorial a laptop with at least two (physical cores) should be sufficient.

## Useful Links

The contents of the tutorials and the tools that are used are accessible via the following links:

- Tutorials gitlab repository, which contains all relevant documents and simulation data: <https://gitlab.com/FHI-aims-club/tutorials/basics-of-running-fhi-aims> 
- Utilities gitlab repository, particularly [CLIMS](https://gitlab.com/FHI-aims-club/utilities/clims): <https://gitlab.com/FHI-aims-club/utilities>
- Detailed instructions for each tutorial can be found at: <https://fhi-aims-club.gitlab.io/tutorials/tutorials-overview/>
- Graphical Interface for Materials Simulation (GIMS): <https://gims.ms1p.org>

We note that the tools used here are general and open-source. GIMS, especially, is intended to support more than just one code since, in our view, it does solve a general problem for our community. It is built upon the [atomic simulation environment (ASE)](https://wiki.fysik.dtu.dk/ase/) and we believe that adding support for other codes will be possible with reasonable effort. A description of GIMS is published here:

Kokott et al., (2021). GIMS: Graphical Interface for Materials Simulations. Journal of Open Source Software, 6(57), 2767. <https://doi.org/10.21105/joss.02767>

## Summary of the Tutorial

### Preparation

- How to obtain and install FHI-aims.
- How to access [GIMS](https://gims.ms1p.org), a very useful graphical user interface which is used for preparation, visualization, and analyzing FHI-aims input files and output data. We strongly advise users to use GIMS throughout the tutorials to visualize the simulation tasks and workflows.

### Outline

1. **Non-spinpolarized, non-periodic** systems (H$_2$O): Understanding in- and output files, performing structure optimization
2. **Spinpolarized, non-periodic** systems (O$_2$): Spin initialization
3. **Periodic** systems (Si and GaAs structure): Relaxation, Post-processing (densities of states, band structure, Mulliken-projected band structure)


Please begin with the ["Preparation chapter"](./preparations.md) to get your hardware ready for FHI-aims! Happy computing!
