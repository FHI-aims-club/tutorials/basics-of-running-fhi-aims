# Summary: A brief overview

Based on the completed tutorial, we here summarize some key aspects of running FHI-aims.

## Main Requirements

To run a simulation with FHI-aims, we need the following pieces:

### FHI-aims executable
  
- The executable can be obtained by compiling the code following the instruction in the FHI-aims manual. A complete overview is also given at the FHI-aims wiki, particularly this page: <https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/wikis/CMake%20Tutorial>
- The FHI-aims binary usually has a name such as `aims.[label].x` where `[label]` is the label of the build, including a version stamp and type of build - for example, `aims.240507.scalapack.mpi.x`.

### Input files

- FHI-aims requires only two input files, called `control.in` and `geometry.in` (please use **EXACTLY** these names), which must be located in the directory from which FHI-aims is called.
- The format of `geometry.in` is a simple and general specification of the nature (chemical element) and type of atomic positions found in a molecule or cluster (non-periodic) or in a three-dimensional periodic representation of a crystal, surface, or nanostructure. A general description of the `geometry.in` file format can be found here: 

    - [Blum, Volker (2020): FHI-aims File Format Description: geometry.in. figshare. Online resource.](https://doi.org/10.6084/m9.figshare.12413477.v1) 
    - Further, FHI-aims specific keywords are described in the [FHI-aims manual](https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/jobs/artifacts/master/raw/doc/manual/FHI-aims.pdf?job=build.manual).

- **`control.in`** contains all other parameters that control the simulation, e.g., the type of the density functional used, tolerances of the simulation, requested tasks and output, etc. 
- Importantly, `control.in` must also contain a description of the numerical parameters ("species defaults", see below) associated with every chemical element (species) found in `geometry.in`. For density-functional theory, these "species defaults" are pre-tabulated at different levels of precision and numerical cost. While they usually do not need to be modified by a user, they do need to be appended in full to the `control.in` file. This practice enables users to modify any numerical parameters used in the simulation, should that be needed - simply by keeping the relevant parameters open and accessible.

### Species defaults: Choice of the numerical settings, including the basis set

- FHI-aims uses numerically tabulated, atom-centered orbital (NAO) basis sets to represent the quantum mechanical orbitals and wave functions. These basis functions are specific to each chemical element ("species") used in the simulation.
-  FHI-aims also employs a variety of other numerical parameters that are specific to each atom and/or chemical element: Angular-momentum expansion order of the electrostatic (Hartree) potential, atom-centered real-space integration grids, etc.
- Predefined files that contain these numerical definitions for each chemical element are located in **`species_defaults`** directory of the FHI-aims code distribution. At least four different levels of precision are provided for each chemical element: "light", "intermediate", "tight", and "really_tight". A more accurate choice of defaults means a higher computational cost of the simulation.
- We note that "light" settings are intended to give qualitatively correct results. However, for "final" publication-quality data, we recommend to obtain all final results at least at the level of "intermediate" species defaults. A full set of "intermediate" species defaults can be found in the directory `species_defaults/defaults_2020/intermediate`. 
- "tight" species defaults for "final" results are recommended particularly if the anticipated results rely on subtle energy differences (this is often the case for problems involving conformations of weakly bonded, flexible molecular species, for example). However, "tight" settings can be unnecessarily expensive particularly for hybrid density functionals and/or for materials with a high density of atoms per volume (e.g., many oxides or nitrides). 
  
### Output files

- By default, the output of FHI-aims is not written to a file but rather to the standard output stream of Unix. In order to capture it in a file, the output stream must be redirected to a file (see below). 
- Unless other output is specifically requested in `control.in`, no other output files are written.
- The standard output stream of FHI-aims is deliberately kept human-readable and it deliberately contains much information, including diagnostic information that can be immensely valuable in order to understand what might have happened in a particular, usually complex simulation. 

  
## ASCII Editors vs. GIMS

`geometry.in` and `control.in` input files, as well as `aims.out` output files can, in principle, be prepared with any text editor capable of ASCII editing. At the Unix command line, the *emacs* editor or the *vim* editor are good tools. Both editors can be fully operated using the keyboard. 

Remember that, on a remote supercomputer, access is usually restricted to keyboard access only, i.e., the fact that both "emacs" or "vim" support keyboard-only operation is an advantage, not a historic aberration. The very instruments that we use for science (i.e., supercomputers) will, for security reasons, not support any other form of access.

Nevertheless, being able to graphically prepare input files before transferring them to a supercomputer and being able to graphically analyze the output after transferring it back to a local computer can be immensely helpful. This is what [GIMS](https://gims.ms1p.org) does. 

GIMS deliberately does not support (nearly) all modes of operation of FHI-aims, but it can simplify a rather important subset. To name a few:

- Build a structure from scratch or import a `geometry.in` file (including support for converting from other formats, such as .cif), modify/manipulate the structure and export to a new  file `geometry.in`.
- Prepare `control.in` files for different types of FHI-aims calculations.
- Import output of the simulation, e.g. `aims.out` file, and visualize key aspects of a simulation, including if and how s.c.f. convergence was achieved, how a structure optimization proceeded, etc.
- Visualize band structures and densities of states of periodic materials, including visualization and manipulation of the Brillouin zone.

![](./images/gims-homepage.png)
  
We will use [GIMS](https://gims.ms1p.org) frequently in the tutorials.

## Important Practical Recommendation

One calculation - one directory.

On the computer intended to run FHI-aims, you will need to manage files using a command line interface and in different folders ("directories") intended to organize your data.

We strongly recommend to create a new directory (with its own control.in and geometry.in files) for every new FHI-aims calculation.

Rerunning and/or continuing FHI-aims calculations in the same directory that was used before will overwrite output and input files. In any simulation (including non-trivial failed attempts), keeping the data can be essential to later understanding and/or reconstructing what happened in a particular calculation.

## How to Execute FHI-aims

After preparing the input files in the simulation folder, e.g. `my-simulation/control.in` and `my-simulation/geometry.in`, a parallel simulation with `N` processes can be launched by typing

```
mpirun -n N aims.x > aims.out 2>&1 
```

or

```
mpirun -n N aims.x | tee aims.out
```

Here we write the output of the simulation to `aims.out` file but you can choose any other name that you prefer. 

The binary name `aims.x` should be replaced with whatever is the name of the FHI-aims binary file compiled by you (including the corresponding path, i.e., the location of the directory in which that file is located). For example, a current binary name might be `~/Software/fhi-aims/bin/aims.240507.scalapack.mpi.x`.

The `mpirun` command facilitates the parallel execution of the code and can have different names that depend on the particular computer system and/or queueing system used. The actual name and usage are usually documented by the computing center in question.

--8<-- "includes/abbreviations.md"
