# Preparations

## 1. Download the Tutorial Material

You can download all of the tutorial material [either as a tarball](https://gitlab.com/FHI-aims-club/tutorials/basics-of-running-fhi-aims/-/archive/master/basics-of-running-fhi-aims-master.tar.gz), or, if you are familiar with git, with the following command:

```
git clone git@gitlab.com:FHI-aims-club/tutorials/basics-of-running-fhi-aims.git
```

## 2. How to install FHI-aims

You will need a valid FHI-aims license for the next steps. Information about how to obtain a license can be found here: [https://fhi-aims.org/get-the-code](https://fhi-aims.org/get-the-code)

We will use the interim release version `240507` for this tutorial. There are two ways of obtaining it: (i) either download the source code as [a tarball](https://fhi-aims.org/verify?file=fhi-aims.240507.tar.gz) or (ii) clone it with git from the FHI-aims GitLab. You will need access to the FHI-aims GitLab to do the latter.

1. Make sure you are in the `FHI-aims-Tutorials` folder.
2. Download the FHI-aims development version via the git command:
   ```
   git clone git@aims-git.rz-berlin.mpg.de:aims/FHIaims.git
   ```
3. Enter the downloaded folder with
   ```
   cd FHI-aims
   ```
   and checkout out the version tag `240507` with
   ```
   git checkout 240507
   ```
4. Follow the install instructions 
    1. on this page: [https://fhi-aims-club.gitlab.io/tutorials/cmake-tutorial/](https://fhi-aims-club.gitlab.io/tutorials/cmake-tutorial/),
    2. or on our wiki (with several templates for different architectures): [https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/wikis/CMake%20Tutorial](https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/wikis/CMake%20Tutorial),
    3. or, alternatively, [download and read the manual](https://aimsclub.fhi-berlin.mpg.de/club_downloads.php).

## 3. How to install clims (optional)

Note that all parts of this tutorial also work without clims.

0. Prerequisites for clims: A working installation of python >= 3.7 is needed. If this presents a problem, most of the tutorial can still be executed without clims.
1. Make sure you are in the `FHI-aims-Tutorials` folder.
2. Install it simply by: `pip install --user clims`
4. Configure clims:
  ```
  clims-configure --species-path $AIMSHOME/FHIaims/species_defaults/defaults_2020
  ```

## 4. How to find GIMS

You can find GIMS here: [https://gims.ms1p.org](https://gims.ms1p.org/static/index.html)
