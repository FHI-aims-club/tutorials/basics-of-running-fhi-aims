# Preparations

## 1. Download the Tutorial Material

You can download all of the tutorial material [either as a tarball](https://gitlab.com/FHI-aims-club/tutorials/basics-of-running-fhi-aims/-/archive/master/basics-of-running-fhi-aims-master.tar.gz), or, if you are familiar with git, with the following command:

```
git clone git@gitlab.com:FHI-aims-club/tutorials/basics-of-running-fhi-aims.git
```

## 2. How to install FHI-aims

You will need a valid FHI-aims license for the next steps. Information about how to obtain a license can be found here: [https://fhi-aims.org/get-the-code](https://fhi-aims.org/get-the-code)

1. Make sure you are in the `FHI-aims-Tutorials` folder.
2. We will use the `210716_1` release for this tutorial series. [Please download it from here.](https://fhi-aims.org/get-the-code-menu/downloads) 
3. Extract the tarball with:
  ```
  tar -xzf fhi-aims.210716_1.tgz
  ```
2. Follow the instructions on this page (you will need access to the FHI-aims GitLab): [https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/wikis/CMake%20Tutorial](https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/wikis/CMake%20Tutorial), or, alternatively, [download and read the manual](https://aimsclub.fhi-berlin.mpg.de/club_downloads.php).

## 3. How to install clims (optional)

Note that all parts of this tutorial also work without clims.

0. Prerequisites for clims: A working installation of python >= 3.7 is needed. If this presents a problem, most of the tutorial can still be executed without clims.
1. Make sure you are in the `FHI-aims-Tutorials` folder.
2. Install it simply by: `pip install --user clims`
4. Configure clims:
  ```
  clims-configure --species-path $AIMSHOME/FHIaims/species_defaults/defaults2020
  ```

## 4. How to find GIMS

You can find GIMS here: [https://gims.ms1p.org](https://gims.ms1p.org/static/index.html)
