# References

FHI-aims is an all-electron electronic structure code that uses localized, numerically tabulated atom-centered basis functions to discretize the orbitals and wave functions of electronic structure theory. This choice enables a high-precision representation of the orbitals and density of complex nano-structures with reasonable computational effort across the periodic table, including all core and valence electrons (no shape approximations to potentials or wave functions). 

Apologies that this list of references became fairly long. And yet, it is only a fairly lopsided fraction of the references we should be listing.

- The construction of basis sets for density-functional theory and the elementary numerical choices made in the implementation are summarized here:

[1] Volker Blum, Ralf Gehrke, Felix Hanke, Paula Havu, Ville Havu, Xinguo Ren, Karsten Reuter, and Matthias Scheffler,
Ab initio molecular simulations with numeric atom-centered orbitals.
Computer Physics Communications 180, 2175-2196 (2009). <http://dx.doi.org/10.1016/j.cpc.2009.06.022>

Importantly, this reference includes the numerical definition of the specific approach to scalar relativistic calculations championed in FHI-aims, i.e., the FHI-aims specific variant of the "atomic zero-order regular approximation (ZORA)". We also note that the methods and concepts summarized in Ref. [1] build heavily on past published methods in our community, particularly the pioneering works of Axel Becke and Bernard Delley.

- Numerical real-space integration in FHI-aims:

[2] Ville Havu, Volker Blum, Paula Havu, and Matthias Scheffler,
Efficient O(N) integration for all-electron electronic structure calculation using numerically tabulated basis functions.
Journal of Computational Physics 228, 8367-8379 (2009). <http://dx.doi.org/10.1016/j.jcp.2009.08.008>

- Basics of handling the two-electron Coulomb operator for exact exchange and correlated methods (GW, RPA) in FHI-aims:

[3] Xinguo Ren, Patrick Rinke, Volker Blum, Jürgen Wieferink, Alex Tkatchenko, Andrea Sanfilippo, Karsten Reuter, and Matthias Scheffler,
Resolution-of-identity approach to Hartree-Fock, hybrid density functionals, RPA, MP2, and GW with numeric atom-centered orbital basis functions.
New Journal of Physics 14, 053020 (2012). <http://stacks.iop.org/1367-2630/14/053020>

- Linear-scaling hybrid density functional theory, including in periodic systems:

[4] Arvid Ihrig, Jürgen Wieferink, Igor Ying Zhang, Matti Ropo, Xinguo Ren, Patrick Rinke, Matthias Scheffler, and Volker Blum
Accurate localized resolution of identity approach for linear-scaling hybrid density functionals and for many-body perturbation theory.
New Journal of Physics 17, 093020 (2015). <http://dx.doi.org/10.1088/1367-2630/17/9/093020>

[5] Sergey Levchenko, Xinguo Ren, Jürgen Wieferink, Rainer Johanni, Patrick Rinke, Volker Blum, Matthias Scheffler
Hybrid functionals for large periodic systems in an all-electron, numeric atom-centered basis framework.
Computer Physics Communications 192, 60-69 (2015). <http://dx.doi.org/10.1016/j.cpc.2015.02.021>

- Stress tensor for periodic systems:

[6] Franz Knuth, Christian Carbogno, Viktor Atalla, Volker Blum and Matthias Scheffler
All-electron Formalism for Total Energy Strain Derivatives and Stress Tensor Components for Numeric Atom-Centered Orbitals.
Computer Physics Communications 190, 33-50 (2015). <http://authors.elsevier.com/sd/article/S0010465515000090>

- Spin-orbit coupling across the periodic table:

[7] William P. Huhn, V. Blum
One-hundred-three compound band-structure benchmark of post-self-consistent spin-orbit coupling treatments in density functional theory.
Physical Review Materials 1, 033803 (2017). <https://doi.org/10.1103/PhysRevMaterials.1.033803>

- Bethe-Salpeter Equation for neutral excitations in molecules:

[8] Chi Liu, Jan Kloppenburg, Yi Yao, Xinguo Ren, Heiko Appel, Yosuke Kanai, Volker Blum
All-electron ab initio Bethe-Salpeter equation approach to neutral excitations in molecules with numeric atom-centered orbitals.
The Journal of Chemical Physics, 152, 044105 (2020).

- Periodic GW for quasiparticle excitations:

[9] Xinguo Ren, Florian Merz, Hong Jiang, Yi Yao, Markus Rampp, Hermann Lederer, Volker Blum and Matthias Scheffler
All-electron periodic G0W0 implementation with numerical atomic orbital basis functions: algorithm and benchmarks.
Physical Review Materials 5, 013807 (2021). <https://doi.org/10.1103/PhysRevMaterials.5.013807> 

- High-performance eigenvalue and density matrix solutions using the ELPA eigenvalue solver and the ELSI electronic structure infrastructure: 

[10] Andreas Marek, Volker Blum, Rainer Johanni, Ville Havu, Bruno Lang, Thomas Auckenthaler, Alexander Heinecke, Hans-Joachim Bungartz, and Hermann Lederer,
The ELPA Library - Scalable Parallel Eigenvalue Solutions for Electronic Structure Theory and Computational Science
The Journal of Physics: Condensed Matter 26, 213201 (2014). <http://stacks.iop.org/0953-8984/26/213201>

[11] Hans-Joachim Bungartz, Christian Carbogno, Martin Galgon, Thomas Huckle, Simone Köcher, Hagen-Henrik Kowalski, Pavel Kus, Bruno Lang, Hermann Lederer, Valeriy Manin, Andreas Marek, Karsten Reuter, Michael Rippl, Matthias Scheffler, Christoph Scheurer,
ELPA: A parallel solver for the generalized eigenvalue problem.
I. Foster et al. (Eds.), Parallel Computing: Technology Trends, 647-668 (IOS Press, 2020).
<https://pure.mpg.de/pubman/item/item_3221243_2/component/file_3221686/APC-36-APC200095.pdf>

[12] Victor Wen-zhe Yu, Fabiano Corsetti, Alberto Garcia, William P. Huhn, Mathias Jacquelin, Weile Jia, Björn Lange, Lin Lin, Jianfeng Lu, Wenhui Mi, Ali Seifitokaldani, Alvaro Vazquez-Mayagoitia, Chao Yang, Haizhao Yang, Volker Blum
ELSI: A Unified Software Interface for Kohn-Sham Electronic Structure Solvers.
Computer Physics Communications 222, 267-285 (2018), DOI: 10.1016/j.cpc.2017.09.007 . <https://doi.org/10.1016/j.cpc.2017.09.007>

[13] Victor Wen-zhe Yu, Carmen Campos, William Dawson, Alberto García, Ville Havu, Ben Hourahine, William P Huhn, Mathias Jacquelin, Weile Jia, Murat Keçeli, Raul Laasner, Yingzhou Li, Lin Lin, Jianfeng Lu, Jonathan Moussa, Jose E Roman, Álvaro Vázquez-Mayagoitia, Chao Yang, Volker Blum
ELSI--An Open Infrastructure for Electronic Structure Solvers.
Computer Physics Communications 256, 107459 (2020). <https://dx.doi.org/10.1016/j.cpc.2020.107459>

[14] Victor Wen-zhe Yu, Jonathan Moussa, Pavel Kůs, Andreas Marek, Peter Messmer, Mina Yoon, Hermann Lederer, Volker Blum
GPU-Acceleration of the ELPA2 Distributed Eigensolver for Dense Symmetric and Hermitian Eigenproblems.
Computer Physics Communications 262, 107808 (2021). <https://doi.org/10.1016/j.cpc.2020.107808>

- GPU acceleration of FHI-aims:

[15] William Huhn, Björn Lange, Victor Wen-zhe Yu, Mina Yoon, Volker Blum
GPGPU Acceleration of All-Electron Electronic Structure Theory Using Localized Numeric Atom-Centered Basis Functions
Computer Physics Communications 254, 107314 (2020). <https://doi.org/10.1016/j.cpc.2020.107314>

- FHI-aims geometry.in file format specification:

[16] Volker Blum. FHI-aims File Format Description: geometry.in. figshare (2020). Online resource. <https://doi.org/10.6084/m9.figshare.12413477.v1> 

- GIMS:

[17] Sebastian Kokott, Iker Hurtado, Christian Vorwerk, Claudia Draxl, Volker Blum, Matthias Scheffler.
GIMS: Graphical Interface for Materials Simulations. Journal of Open Source Software, 6(57), 2767. <https://doi.org/10.21105/joss.02767>

