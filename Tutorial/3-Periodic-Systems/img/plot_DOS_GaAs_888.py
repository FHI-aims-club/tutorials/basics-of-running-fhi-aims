#!/usr/bin/env python
#

import numpy as np
from matplotlib import rcParams
import matplotlib.pyplot as plt

rcParams.update({'font.size': 14})
rcParams['lines.linewidth'] = 2.5

data_gauss1 = np.loadtxt("../solutions/GaAs/bands_dos_SOC/KS_DOS_total.dat",skiprows=3)
data_gauss2 = np.loadtxt("../solutions/GaAs_DOS/1_8x8x8_Gauss_dos_kgrid_factors_4x4x4_broad_0.1/KS_DOS_total.dat",skiprows=3)

fig, ax = plt.subplots(figsize=(12,8))

ax.plot(data_gauss1[:,0],data_gauss1[:,1],label="Gaussian DOS, k-grid 8x8x8",c='k')
ax.plot(data_gauss2[:,0],data_gauss2[:,1],label="Gaussian DOS, k-grid 8x8x8, dos_kgrid_factors 4x4x4",c="tab:orange")

ax.set_xlabel("Energy (eV)")
ax.set_ylabel("Density of States")
ax.set_xlim(-5,5)
ax.legend(loc=(0.3,0.83))

fig.savefig("DOS_GaAs_888.png")

plt.show()
