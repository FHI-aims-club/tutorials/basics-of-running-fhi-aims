#!/usr/bin/env python
#

import numpy as np
from matplotlib import rcParams
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, mark_inset

rcParams.update({'font.size': 14})
rcParams['lines.linewidth'] = 2.5

data_gauss1 = np.loadtxt("../solutions/GaAs_DOS/2_12x12x12_Gauss_dos_kgrid_factors_4x4x4_broad_0.05/KS_DOS_total.dat",skiprows=3)
data_gauss2 = np.loadtxt("../solutions/GaAs_DOS/1_8x8x8_Gauss_dos_kgrid_factors_4x4x4_broad_0.1/KS_DOS_total.dat",skiprows=3)

fig, ax = plt.subplots(figsize=(12,8))

ax.plot(data_gauss2[:,0],data_gauss2[:,1],label="Gaussian DOS, k-grid 8x8x8, dos_kgrid_factors 4x4x4, Broad. 0.1 eV",c="tab:orange")
ax.plot(data_gauss1[:,0],data_gauss1[:,1],label="Gaussian DOS, k-grid 12x12x12, dos_kgrid_factors 4x4x4, Broad 0.05 eV",c="tab:green")

axins = inset_axes(ax, width="30%", height="30%",loc="center")

axins.set_xlim(-0.7, 1.2)
axins.set_ylim(-0.01, 0.1)
axins.plot(data_gauss2[:,0],data_gauss2[:,1],c="tab:orange")
axins.plot(data_gauss1[:,0],data_gauss1[:,1],c="tab:green")
axins.set_xticks([])
axins.set_yticks([])

mark_inset(ax, axins, loc1=3, loc2=4, fc="none", ec="0.5")

ax.set_xlabel("Energy (eV)")
ax.set_ylabel("Density of States")
ax.set_xlim(-5,5)
ax.legend(loc=(0.1,0.83))

fig.savefig("DOS_GaAs_121212-888.png")

plt.show()
