#!/usr/bin/env python
#

import numpy as np
from matplotlib import rcParams
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, mark_inset

rcParams.update({'font.size': 14})
rcParams['lines.linewidth'] = 2.5

data_tet2 = np.loadtxt("../solutions/GaAs_DOS/4_12x12x12_tetrahedron_dos_kgrid_factors_2x2x2/KS_DOS_total_tetrahedron.dat",skiprows=3)
data_tet1 = np.loadtxt("../solutions/GaAs_DOS/3_12x12x12_tetrahedron/KS_DOS_total_tetrahedron.dat",skiprows=3)

fig, ax = plt.subplots(figsize=(12,8))

ax.plot(data_tet1[:,0],data_tet1[:,1],label="Tetrahedron DOS, k-grid 12x12x12",c="tab:blue")
ax.plot(data_tet2[:,0],data_tet2[:,1],label="Tetrahedron DOS, k-grid 12x12x12, dos_kgrid_factors 2x2x2",c="tab:red")

ax.set_xlabel("Energy (eV)")
ax.set_ylabel("Density of States")
ax.set_xlim(-5,5)
ax.legend(loc=(0.1,0.83))

fig.savefig("DOS_GaAs_121212-tet.png")

plt.show()
