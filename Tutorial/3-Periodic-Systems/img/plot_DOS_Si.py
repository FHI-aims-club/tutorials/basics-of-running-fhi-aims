#!/usr/bin/env python
#

import numpy as np
from matplotlib import rcParams
import matplotlib.pyplot as plt

rcParams.update({'font.size': 14})
rcParams['lines.linewidth'] = 2.5

data_gauss1 = np.loadtxt("../solutions/Si/HSE06_bands_dos/KS_DOS_total.dat",skiprows=3)
data_gauss2 = np.loadtxt("../solutions/Si/HSE06_dos_kgrid_factors/KS_DOS_total.dat",skiprows=3)

fig, ax = plt.subplots(figsize=(12,8))

ax.plot(data_gauss1[:,0],data_gauss1[:,1],label="Gaussian DOS, k-grid 12x12x12",c='k')
ax.plot(data_gauss2[:,0],data_gauss2[:,1],label="Gaussian DOS, k-grid 12x12x12, dos_kgrid_factors 3x3x3",c="tab:orange")

ax.set_xlabel("Energy (eV)")
ax.set_ylabel("Density of States")
ax.set_xlim(-5,5)
ax.legend(loc=(0.1,0.83))

fig.savefig("dos_kgrid_factors_Si.png")

plt.show()
