As an introduction to this topic we also recorded two lectures. Please find them below. The corresponding lecture slides can be found here:

* [Matthias Scheffler: Introduction to Electronic Structure Theory](https://indico.fhi-berlin.mpg.de/event/112/contributions/618/attachments/229/724/Scheffler%20Matthias%20FHI-aims%20Talk%20August%202021.pdf)
* [Volker Blum: Numeric Atom-centered Orbitals and How to Use them](https://indico.fhi-berlin.mpg.de/event/112/contributions/619/attachments/235/727/FHI-aims%202021.pdf)

## Matthias Scheffler: Introduction to Electronic Structure Theory

<iframe width="560" height="315" src="https://www.youtube.com/embed/-44gxRfHktk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Volker Blum: Numeric Atom-centered Orbitals and How to Use them

<iframe width="560" height="315" src="https://www.youtube.com/embed/yCPGRGrOLgo?start=9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>